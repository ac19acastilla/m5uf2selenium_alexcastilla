package Selenium;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.concurrent.TimeUnit;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author rvallez
 */
public class SeleniumPHP {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "./src/test/java/resources/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://localhost/p2m5uf2/");
    }

    @Test
    public void diametroCirculoTest() throws Exception {       
        //get el checbox diametro y lo clicas
        WebElement checkboxDiametro = driver.findElement(By.id("diametro"));
        checkboxDiametro.click();
        
        //select figura
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(1);
        
        // TEXTO RADIO		
        WebElement textRadio = driver.findElement(By.id("radio"));
        textRadio.sendKeys("4");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }
    
    @Test
    public void areaCirculoTest() throws Exception {
        //get el checbox area y lo clicas
        WebElement checkboxArea = driver.findElement(By.id("area"));
        checkboxArea.click();
        
        //select figura
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(1);
        
        // TEXTO RADIO		
        WebElement textRadio = driver.findElement(By.id("radio"));
        textRadio.sendKeys("4");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }
    
    @Test
    public void cuadradoAreaTest() throws Exception {
        //get el checbox area y lo clicas
        WebElement checkboxArea = driver.findElement(By.id("area"));
        checkboxArea.click();
        
        //select figura
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(2);
        
        // TEXTO base		
        WebElement textBase = driver.findElement(By.id("base"));
        textBase.sendKeys("5");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }
    
    @Test
    public void cuadradoPerimetroTest() throws Exception {
        //get el checbox area y lo clicas
        WebElement checkboxArea = driver.findElement(By.id("diametro"));
        checkboxArea.click();
        
        //select figura
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(2);
        
        // TEXTO base		
        WebElement textBase = driver.findElement(By.id("base"));
        textBase.sendKeys("5");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }
    
    @Test
    public void trianguloAreaTest() throws Exception {
        //get el checbox area y lo clicas
        WebElement checkboxArea = driver.findElement(By.id("area"));
        checkboxArea.click();
        
        //select figura
        //No se por qué, pero el indix 3 que es el del triangulo, no me lo coge
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(3);
        
        // TEXTO base		
        WebElement textBase = driver.findElement(By.id("base"));
        textBase.sendKeys("5");
        
        // TEXTO altura		
        WebElement textAltura = driver.findElement(By.id("altura"));
        textAltura.sendKeys("10");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }
    
    @Test
    public void trianguloPerimetroTest() throws Exception {
        //get el checbox area y lo clicas
        WebElement checkboxArea = driver.findElement(By.id("diametro"));
        checkboxArea.click();
        
        //select figura
        Select selectFigura = new Select(driver.findElement(By.id("figura")));
	selectFigura.selectByIndex(3);
        
        // TEXTO base		
        WebElement textBase = driver.findElement(By.id("base"));
        textBase.sendKeys("5");
        
        // TEXTO altura		
        WebElement textAltura = driver.findElement(By.id("altura"));
        textAltura.sendKeys("10");
        
        //submit
        driver.findElement(By.id("submit")).submit();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
